# Geo Spring
Geolocation SpringBoot REST API application with Docker.

# Docker
1. Run build task of Maven install
   - `mvn -B install`
2. Build docker image
   - `docker build -t geo-spring.jar .`
3. Run the docker image
   - `docker run -p 9090:8080 geo-spring.jar`