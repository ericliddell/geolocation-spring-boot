package com.ericsgiovano.geo.GeoSpring;

import com.ericsgiovano.geo.GeoSpring.service.LocationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.ericsgiovano.geo.GeoSpring.entity.Location;

@Component
public class DataLoader implements ApplicationRunner {

    @Autowired
    private LocationService locationService;

    public void run(ApplicationArguments args) {
        List<Location> locationList = new ArrayList<>();

        for (int i = 0; i < 200; i++) {
            locationList.add(new Location(locationList.size(), "Test1", "21CNR", new ArrayList<>(Arrays.asList(1.359167, 103.989441))));
            locationList.add(new Location(locationList.size(), "Test2", "28CNR", new ArrayList<>(Arrays.asList(1.3488196938853985, 103.97052082639239))));
            locationList.add(new Location(locationList.size(), "Test3", "ARC", new ArrayList<>(Arrays.asList(1.2969133990837274, 103.78782071290127))));
            locationList.add(new Location(locationList.size(), "Test3", "21CNR", new ArrayList<>(Arrays.asList(1.359167, 103.989441))));
            locationList.add(new Location(locationList.size(), "Test34", "21CNR", new ArrayList<>(Arrays.asList(1.359167, 103.989441))));
        }

        locationService.setLocationList(locationList);
    }

}
