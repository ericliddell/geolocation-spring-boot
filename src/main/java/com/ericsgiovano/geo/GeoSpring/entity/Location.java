package com.ericsgiovano.geo.GeoSpring.entity;

import java.util.List;

import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Location {

    private Integer id;

    @Size(min = 3, max = 10)
    private String name;

    @Size(min = 3, max = 10)
    private String location;

    @Size(max = 2)
    private List<Double> geoPoint;

}
