package com.ericsgiovano.geo.GeoSpring.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MatchView {

    private Integer exactMatchCount;
    private Integer matchCount;

}
