package com.ericsgiovano.geo.GeoSpring.entity;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LocationView {

    private Integer id;
    private String name;
    private List<Double> geoPoint;

}
