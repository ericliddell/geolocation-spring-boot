package com.ericsgiovano.geo.GeoSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeoSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeoSpringApplication.class, args);
	}

}
