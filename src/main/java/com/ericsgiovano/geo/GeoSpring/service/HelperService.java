package com.ericsgiovano.geo.GeoSpring.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HelperService {
    public static double getDistance(double lat1, double lon1, double lat2, double lon2) {
        // Haversine Great-circle distance approximation, returns meters
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60; // 60 nautical miles per degree of seperation
        dist = dist * 1852; // 1852 meters per nautical mile

        // Convert to KM
        dist = dist / 1000;
        return (dist);
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    public static boolean isLatitudeValid(double userLatitude){
        return userLatitude < -90 || userLatitude > 90; 
    }

    public static boolean isLongtitudeValid(double userLongitude){
        return userLongitude < -180 || userLongitude > 180;
    }
    
    public static boolean isExactMatch(String source, String subItem) {
        String pattern = "\\b" + subItem + "\\b";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(source);
        return m.find();
    }

    public static boolean isContainMatchNonCaseSenstive(String source, String subItem) {
        return source.toLowerCase().contains(subItem.toLowerCase());
    }
}
