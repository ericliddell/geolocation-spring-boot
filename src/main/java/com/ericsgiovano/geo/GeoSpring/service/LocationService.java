package com.ericsgiovano.geo.GeoSpring.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import com.ericsgiovano.geo.GeoSpring.entity.Location;
import com.ericsgiovano.geo.GeoSpring.entity.LocationView;
import com.ericsgiovano.geo.GeoSpring.entity.MatchView;
import com.ericsgiovano.geo.GeoSpring.utils.BadRequestException;
import com.ericsgiovano.geo.GeoSpring.utils.NotFoundException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public class LocationService {

    @Value("${search-perimeter-in-km}")
    private Integer definedSearchPerimeterInKM;

    private List<Location> locationList;

    public HashMap<String, List<LocationView>> getAllLocationListByLocationCode() {
        HashMap<String, List<LocationView>> locationsByCodeMap = new HashMap<String, List<LocationView>>();

        for (Location location : locationList) {
            if (locationsByCodeMap.containsKey(location.getLocation())) {
                List<LocationView> locationByCodeList = locationsByCodeMap.get(location.getLocation());

                LocationView locationView = new LocationView(
                    location.getId(),
                    location.getName(),
                    location.getGeoPoint()
                );
                locationByCodeList.add(locationView);
            } else {
                List<LocationView> locationByCodeList = new ArrayList<LocationView>();

                LocationView locationView = new LocationView(
                    location.getId(),
                    location.getName(),
                    location.getGeoPoint()
                );
                locationByCodeList.add(locationView);
                locationsByCodeMap.put(location.getLocation(), locationByCodeList);
            }
        }

        return locationsByCodeMap;
    }

    public List<Location> findLocationsByPerimeter(double userLatitude, double userLongitude) {
        if(HelperService.isLatitudeValid(userLatitude)){
            throw new BadRequestException("Latitude must be between -90 and 90, as for submitted latitude is " + userLatitude);
        }

        if(HelperService.isLongtitudeValid(userLongitude)){
            throw new BadRequestException("Longitude must be between -180 and 180, as for submitted longtitude is " + userLongitude);
        }

        List<Location> locationsWithinPerimiter = new ArrayList<Location>();

        for (Location location : locationList) {
            if(HelperService.getDistance(
                userLatitude, 
                userLongitude, 
                location.getGeoPoint().get(0), 
                location.getGeoPoint().get(1)) < definedSearchPerimeterInKM)
            {
                locationsWithinPerimiter.add(location);
            }
        }

        return locationsWithinPerimiter;
    }

    public MatchView findMatchingLocations(String searchLocationTerm) {
        MatchView matchCount = new MatchView();
        matchCount.setExactMatchCount(0);
        matchCount.setMatchCount(0);

        for (Location location : locationList) {
            // Check for exact match
            if (HelperService.isExactMatch(location.getName(), searchLocationTerm)) {
                matchCount.setExactMatchCount(matchCount.getExactMatchCount() + 1);
                matchCount.setMatchCount(matchCount.getMatchCount() + 1);
            } else if (HelperService.isContainMatchNonCaseSenstive(location.getName(), searchLocationTerm)) {
                matchCount.setMatchCount(matchCount.getMatchCount() + 1);
            }
        }

        return matchCount;
    }

    public Location updateLocation(Location newLocation, Integer id) {
        Location location = getLocationById(id);

        if(Objects.isNull(location)){
            throw new NotFoundException("Location not found with ID: " + id);
        }

        if(!Objects.isNull(newLocation.getGeoPoint().get(0)) &&
            HelperService.isLatitudeValid(newLocation.getGeoPoint().get(0)))
        {
            throw new BadRequestException("Latitude must be between -90 and 90, as for submitted latitude is " + newLocation.getGeoPoint().get(0));
        }

        if(!Objects.isNull(newLocation.getGeoPoint().get(1)) &&
            HelperService.isLongtitudeValid(newLocation.getGeoPoint().get(1)))
        {
            throw new BadRequestException("Longitude must be between -180 and 180, as for submitted longtitude is " + newLocation.getGeoPoint().get(1));
        }

        if (!Objects.isNull(newLocation.getName()) &&
            !"".equals(newLocation.getName())) {
            location.setName(newLocation.getName());
        }

        if (!Objects.isNull(newLocation.getLocation()) &&
            !"".equals(newLocation.getLocation())) {
            location.setLocation(newLocation.getLocation());
        }

        if (!newLocation.getGeoPoint().isEmpty()) {
            location.setGeoPoint(
                new ArrayList<Double>(
                    Arrays.asList(
                        newLocation.getGeoPoint().get(0),
                        newLocation.getGeoPoint().get(1)
                    )
                )
            );
        }

        return location;
    }

    public Location getLocationById(Integer id){
        for (Location location : locationList) {
            if (location.getId().equals(id)){
                return location;
            }
        }
        return null;
    }

}
