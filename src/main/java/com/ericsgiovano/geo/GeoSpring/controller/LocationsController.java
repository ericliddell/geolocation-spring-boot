package com.ericsgiovano.geo.GeoSpring.controller;

import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import com.ericsgiovano.geo.GeoSpring.entity.Location;
import com.ericsgiovano.geo.GeoSpring.entity.LocationView;
import com.ericsgiovano.geo.GeoSpring.entity.MatchView;
import com.ericsgiovano.geo.GeoSpring.service.LocationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/locations")
@Validated
public class LocationsController {

    @Autowired
    private LocationService locationService;

    @GetMapping
    public HashMap<String, List<LocationView>> getAll() {
        return locationService.getAllLocationListByLocationCode();
    }

    @PostMapping("/locationswithinperimeter")
    public List<Location> findLocationsByPerimeter(
        @RequestBody 
        @Size(min = 2, max = 2)
        List<Double> userLocation
    ){
        double userLatitude = userLocation.get(0);
        double userLongitude = userLocation.get(1);
        return locationService.findLocationsByPerimeter(userLatitude, userLongitude);
    }

    @GetMapping("/matchinglocations")
    public MatchView findMatchingLocations(
        @RequestParam("searchLocationTerm") 
        @NotBlank 
        @Size(min = 3, max = 10)
        String searchLocationTerm
    ){
        return locationService.findMatchingLocations(searchLocationTerm);
    }

    @PutMapping("/{id}")
    public Location updateLocation(
        @RequestBody 
        @Valid Location newLocation,
        @PathVariable("id")
        @NotNull
        @Positive
        Integer id
    ){
        return locationService.updateLocation(newLocation, id);
    }

}
