FROM adoptopenjdk/openjdk11:latest
EXPOSE 8080
ADD target/geo-spring.jar geo-spring.jar
ENTRYPOINT ["java","-jar","/geo-spring.jar"]